# README #

### What is this repository for? ###
* Version - 1.0
* Quick summary - This repository uses regex and finite state automata to parse any given url into various components i.e protocol, host, port, path param and query param

For example, for the url: "http://abc.com:39000/hello?boo=ff"
Scheme - "http"
Host - "abc.com"
Port - 39000
Path - "hello"
Query - "boo=ff"

* For build and tests - "mvn clean package"

* Assumptions - 
     1. protocol/scheme - http or https only
     2. Url will not contain special characters

* Other details - 

* UrlParserComparison - Class comparing url parsing technique based on regex and finite state machine.
* UrlComponentParserRegex - Class which uses regex pattern to identify various components of url.
* UrlComponentParser - Class which runs a finite state machine algorithm to identify various components of url. This * finite state machine has many states, mostly based on the parts of a URL i.e protocol, port, host. The state is kept by a series of boolean variables, shifting from state to state, consuming one character at a time.


