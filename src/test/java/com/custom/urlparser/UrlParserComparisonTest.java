package com.custom.urlparser;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UrlParserComparisonTest {

	@DataProvider
	public Object[][] getUrls() {
		return new Object[][] { { "http://host:8090/path?params", "" }, { "http://host/path?params", "" },
				{ "http://host/path", "" }, { "https://www.google.com/search/patanahi?q='abc'", "" },
				{ "http://example.com:80/docs/books/tutorial/index.html?name=networking#DOWNLOADING", "" } };
	}

	@Test(dataProvider = "getUrls")
	public void compareUrlParsers(String url, String dummyValue) {
		for (int i = 0; i < 10000; i++) {
			UrlParserComparison.parseUrlUsingRegex(url);
			UrlParserComparison.parseUrlUsingState(url);
		}
	}

}
