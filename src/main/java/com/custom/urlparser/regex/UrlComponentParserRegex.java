package com.custom.urlparser.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.custom.urlparser.UrlComponent;

/*Class which uses regex pattern to identify various components of url*/
public class UrlComponentParserRegex {

	private static final String URL_PATTERN_REGEX = "(https?)://([^:^/]*):?(\\d*)?/([^\\?]*)?[\\?]?(.*)?";

	public UrlComponent parse(String url) {
		UrlComponent urlComponent = new UrlComponent();
		Pattern pattern = Pattern.compile(URL_PATTERN_REGEX);
		Matcher matcher = pattern.matcher(url);
		boolean isPatternFound = matcher.find();
		if (isPatternFound) {
			String protocol = matcher.group(1);
			if (isNotNullNotEmpty(protocol)) {
				urlComponent.setProtocol(protocol);
			}

			String host = matcher.group(2);
			if (isNotNullNotEmpty(host)) {
				urlComponent.setHost(host);
			}

			String port = matcher.group(3);
			if (isNotNullNotEmpty(port)) {
				urlComponent.setPort(port);
			}

			String pathParam = matcher.group(4);
			if (isNotNullNotEmpty(pathParam)) {
				urlComponent.setPathParam(pathParam);
			}

			String queryParam = matcher.group(5);
			if (isNotNullNotEmpty(queryParam)) {
				urlComponent.setQueryString(queryParam);
			}

		}
		return urlComponent;
	}

	private boolean isNotNullNotEmpty(String input) {
		return input != null && !input.isEmpty();
	}

}
