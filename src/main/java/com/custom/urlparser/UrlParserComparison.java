package com.custom.urlparser;

import java.io.InputStreamReader;
import java.util.Scanner;

import com.custom.urlparser.finitestate.UrlComponentParser;
import com.custom.urlparser.regex.UrlComponentParserRegex;

/*Class comparing url parsing technique based on regex and finite state machine. */
public class UrlParserComparison {

	public static void parseUrlUsingRegex(String url) {
		long start = System.currentTimeMillis();
		UrlComponentParserRegex urlComponentParserRegex = new UrlComponentParserRegex();
		UrlComponent urlComponent = urlComponentParserRegex.parse(url);
		long endTime = (System.currentTimeMillis() - start);

		System.out.println(urlComponent.getProtocol());
		System.out.println(urlComponent.getHost());
		System.out.println(urlComponent.getPort());
		System.out.println(urlComponent.getPathParam());
		System.out.println(urlComponent.getQueryString());

		System.out.println("Time taken by Regex: " + endTime + " millisec");
	}

	public static void parseUrlUsingState(String url) {
		long start = System.currentTimeMillis();
		UrlComponentParser parser = new UrlComponentParser(url);
		UrlComponent urlComponent = parser.parse();
		long endTime = (System.currentTimeMillis() - start);

		System.out.println(urlComponent.getProtocol());
		System.out.println(urlComponent.getHost());
		System.out.println(urlComponent.getPort());
		System.out.println(urlComponent.getPathParam());
		System.out.println(urlComponent.getQueryString());

		System.out.println("Time taken by State: " + endTime + " millisec");

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		System.out.println("Please enter url: ");
		String input = scanner.nextLine();
		for (int i = 0; i < 10000; i++) {
			parseUrlUsingRegex(input);
			parseUrlUsingState(input);
		}
		scanner.close();
	}
}
