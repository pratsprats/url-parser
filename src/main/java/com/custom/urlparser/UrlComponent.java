package com.custom.urlparser;

/* Response Object containing all the url components */
public class UrlComponent {

	private String protocol;
	private String port;
	private String host;
	private String queryString;
	private String pathParam;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public String getPathParam() {
		return pathParam;
	}

	public void setPathParam(String pathParam) {
		this.pathParam = pathParam;
	}

	@Override
	public String toString() {
		return "UrlComponent [" + (protocol != null ? "scheme=" + protocol + ", " : "")
				+ (port != null ? "port=" + port + ", " : "") + (host != null ? "host=" + host + ", " : "")
				+ (queryString != null ? "queryString=" + queryString + ", " : "")
				+ (pathParam != null ? "pathParam=" + pathParam : "") + "]";
	}

}
