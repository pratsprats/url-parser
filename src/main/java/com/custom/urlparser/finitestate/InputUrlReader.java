package com.custom.urlparser.finitestate;

/*Wrapper class which accepts the input string, converts it to array of characters 
  and also keeps track of current read index. */
public class InputUrlReader {

	private char[] inputUrlCharArray;
	private int currentPosition;

	public InputUrlReader(String urlString) {
		inputUrlCharArray = urlString.toCharArray();
	}

	public char getCurrentChar() {
		return inputUrlCharArray[currentPosition++];
	}

	public boolean isEndOfInputUrl() {
		return currentPosition >= inputUrlCharArray.length;
	}

	public void forceEnd() {
		currentPosition = inputUrlCharArray.length;
	}

	public int getCurrentPosition() {
		return currentPosition;
	}

	public void jumpToPreviousPosition() {
		currentPosition--;
	}

	public String getInputUrlString() {
		return new String(inputUrlCharArray);
	}

}
