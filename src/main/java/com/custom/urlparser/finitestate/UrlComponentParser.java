package com.custom.urlparser.finitestate;

import java.util.Arrays;
import java.util.List;

import com.custom.urlparser.UrlComponent;

/* Class which runs a finite state machine algorithm to identify various components of url
 * This finite state machine has many states, mostly based on the parts of a URL i.e protocol, port, host. 
 * The state is kept by a series of boolean variables, shifting from state to state, 
 * consuming one character at a time */
public class UrlComponentParser {
	private static final List<String> VALID_PROTOCOL = Arrays.asList("http://", "https://");
	private static final UrlComponent EMPTY_RESPONSE = new UrlComponent();

	private boolean protocolFound = false;
	private boolean portFound = false;
	boolean hostFound = false;

	private UrlComponent urlComponent;
	private InputUrlReader reader;
	private StringBuilder buffer;

	public UrlComponentParser(String url) {
		reader = new InputUrlReader(url);
	}

	/*
	 * Method which takes care of parsing the input url and returns UrlComponent
	 * object
	 */
	public UrlComponent parse() {
		urlComponent = new UrlComponent();
		buffer = new StringBuilder();

		while (!reader.isEndOfInputUrl()) {
			char curr = reader.getCurrentChar();

			switch (curr) {
			case ':':
				buffer.append(curr);
				boolean isValidUrl = processColon();
				if (!isValidUrl) {
					return EMPTY_RESPONSE;
				}
				break;
			case '/':
				buffer.append(curr);
				processPathParam();
				break;

			case '?':
				buffer.append(curr);
				processQueryParam();
				break;

			default:
				buffer.append(curr);
				break;
			}

			// /*
			// * Once we have identified the protocol, we need to find the host
			// * information
			// */
			// if (protocolFound && !hostFound) {
			// boolean isValidUrl = processHost();
			// if (!isValidUrl) {
			// return EMPTY_RESPONSE;
			// }
			// }
		}

		return urlComponent;
	}

	/*
	 * The first colon will indicate protocol end and the second one will
	 * indicate start of port number. Any other colon in URL will be treated as
	 * bad request
	 */
	private boolean processColon() {
		boolean isValidUrl = false;

		if (!protocolFound && buffer.length() > 0 && findProtocol()) {
			protocolFound = true;
			isValidUrl = true;

			/*
			 * Once we have identified the protocol, we need to find the host
			 * information
			 */
			if (!hostFound) {
				isValidUrl = processHost();
			}
		} else if (!portFound && buffer.length() > 0 && findPort()) {
			portFound = true;
			isValidUrl = true;
		}

		return isValidUrl;
	}

	/*
	 * Method to check if url pattern contains protocol information or not,
	 * making use of the fact that protocols are suffixed by '://'
	 */
	private boolean findProtocol() {
		int numSlashes = 0;

		while (!reader.isEndOfInputUrl()) {
			char curr = reader.getCurrentChar();

			if (curr == '/') {
				buffer.append(curr);
				/*
				 * If the num of backward slashes are 2, then it means we have
				 * found protocol
				 */
				if (numSlashes == 1) {
					/*
					 * Assuming only http and https as valid protocols for this
					 * execrise
					 */
					if (VALID_PROTOCOL.contains(buffer.toString().toLowerCase())) {
						urlComponent.setProtocol(buffer.substring(0, buffer.length() - 3));
						return true;
					}
					return false;
				}
				numSlashes++;
			} else {
				// reader.jumpToPreviousPosition();
				return false;
			}
		}
		return false;
	}

	/*
	 * Method to check if url pattern contains port information or not, making
	 * use of the fact that port is prefixed by ':' and is numeric
	 */
	private boolean findPort() {
		int startIndex = buffer.length();
		int numDigits = 0;

		while (!reader.isEndOfInputUrl()) {
			char curr = reader.getCurrentChar();

			if (isNumeric(curr)) {
				buffer.append(curr);
				numDigits++;
			} else if (curr == '/') {
				if (numDigits == 0) {
					// invalid url
					return false;
				}
				urlComponent.setPort(buffer.substring(startIndex, reader.getCurrentPosition() - 1));
				reader.jumpToPreviousPosition();
				return true;
			} else {
				// invalid url
				return false;
			}
		}
		return false;
	}

	/*
	 * Method to check if url pattern contains host information or not, making
	 * use of the fact that host is suffixed either by ':' or '/'
	 */
	private boolean processHost() {
		int startIndex = buffer.length();

		while (!reader.isEndOfInputUrl()) {
			char curr = reader.getCurrentChar();

			// if we match a slash or colon, we have reached to the end of host
			if (curr == '/' || curr == ':') {
				if (startIndex == buffer.length()) {
					// invalid url
					return false;
				}
				urlComponent.setHost(buffer.substring(startIndex, buffer.length()));
				hostFound = true;
				reader.jumpToPreviousPosition();
				return true;
			} else {
				buffer.append(curr);
			}
		}
		return false;
	}

	/*
	 * Method to check if url pattern contains query param information or not,
	 * making use of the fact that query param is prefixed by '?' and contains
	 * information till end of url.
	 */
	private void processQueryParam() {
		urlComponent.setQueryString(reader.getInputUrlString().substring(buffer.length()));
		/*
		 * Forcing reader to close the loop since we have already reached to the
		 * end of url
		 */
		reader.forceEnd();
	}

	/*
	 * Method to check if url pattern contains path information or not, making
	 * use of the fact that path is prefixed by '/' and suffixed either by '?'
	 * or end of url.
	 */
	private void processPathParam() {
		int startIndex = buffer.length();

		while (!reader.isEndOfInputUrl()) {
			char curr = reader.getCurrentChar();

			/* if we match a ?, it means this url will also have query string */
			if (curr == '?') {
				/*
				 * Setting path param in case url also contains a query string
				 */
				urlComponent.setPathParam(buffer.substring(startIndex, buffer.length()));
				reader.jumpToPreviousPosition();
				return;
			} else {
				buffer.append(curr);
			}
		}
		/* Setting path param in case url does not have query string */
		urlComponent.setPathParam(buffer.substring(startIndex, buffer.length()));
	}

	/* Utility method to find out if a given character is numeric or not. */
	public static boolean isNumeric(char a) {
		return a >= '0' && a <= '9';
	}

}
